# Market Abuse Detection

In this task we need to retrieve stock data for Amazon and analyse some client 
data in order to find some suspicious behaviours. 

## Data description
- The file `traders_data.csv` includes some traders orders data. All those trades only reflect order
submissions, which may have been filled or not.
- Amazon stock data should be retrieved from Yahoo for the months of February and March 2020
using the library `pandas_datareader`. 

## Task
We want to find traders which made suspicious orders. To be a suspicious orders we consider the 
following rules:
- The trader has submitted an order above the high price/below the low price for a given day of a stock
- The trader has submitted an order in a date when the stock was not traded

If any suspicious orders are found, we want to do the following analysis:

- If more than one trader is found, rank by number of suspicious orders per trader.
- Try to find if there is a correlation between the nationality of the trader and the 
tendency to make suspicious orders 

## Output
Output is stored in a CSV file.

Analaysis Outcome:
Full_name	    Country_code	Suspicous_order_count
Scott Thompson	North Macedonia	  7
John Nguyen	    Comoros	          6
Tonya Hernandez	 Korea, Democratic People's Republic of	 5
-If you see top 3  coutries people are from very poor countries, so according to me it depends on wealth of the country.




