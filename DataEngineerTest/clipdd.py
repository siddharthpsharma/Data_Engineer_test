import pandas as pd
import numpy as np
import pandas_datareader as dr
import pycountry
import matplotlib.pyplot as plt

'''Reading dataset'''
def read_data(csv_path,start_date,end_date,stock_syb):
    """Get location and return dataframe

     Parameters
     ----------
     csv_path : str
         The file location of the spreadsheet
    strat_date & end_date : Date
     Returns
     -------
     dataframe
         contains the data of csv
     """
    try:
        df=pd.read_csv(csv_path)
        df=df[(df['tradeDatetime'] >= start_date) & (df['tradeDatetime'] <= end_date) & (df['stockSymbol'] == stock_syb)].sort_values(by='tradeDatetime')
    except Exception:
        print("Something went wrong in read_csv ")
        exit("return code 1")
    return  df

def get_yahoo_data(stock_syb,start_date,end_date):
    """Get data from Yahoo and return dataframe

         Parameters
         ----------
          stock_syb: str
            Stock Sybmbol eaxmple:AMZN
          Start_date:Date
          Start date from were you want data
          end_date: Date
          TIll what date you want data
         Returns
         -------
         dataframe
         """
    try:
        df_2 = dr.data.get_data_yahoo(stock_syb, start=start_date, end=end_date)
        df_2.reset_index(inplace=True)
    except Exception:
        print("Sorry, Something went wrong in get_yahoo_data")
        exit("return code 1")
    return  df_2

def data_cleaning(data_traders):
    """Cleaning of Data
             """
    try:
        data_traders = data_traders.drop('volume', axis=1)
        data_traders.dropna(inplace=True)
        data_traders['tradeDatetime'] = pd.to_datetime(pd.to_datetime(data_traders['tradeDatetime']).dt.date)
    except Exception:
        print("Sorry, Something went wrong in data_cleaning")
        exit("return code 1")
    return  data_traders


def merging_frames(data_traders,data_yahoo):
    """Merging tow data frames
                 """
    try:
        df_total = data_traders.merge(data_yahoo, left_on='tradeDatetime', right_on='Date', how='left')
    except Exception:
        print("Sorry, Something went wrong in merging_frames")
        exit("return code 1")
    return df_total

def predictions_of_fraud(data_traders,data_yahoo):
    """Predictions of Fraud or suspicious orders
                 """
    try:
        merg_df=merging_frames(data_traders, data_yahoo)
        merg_df=merg_df[(merg_df['Date'].isnull()) | ((merg_df["price"] > merg_df["High"]) | (merg_df["price"] < merg_df["Low"]))]
        merg_df["full_name"]=merg_df["full_name"]= merg_df.firstName+" "+merg_df.lastName
        df_viz=pd.DataFrame(merg_df[["full_name","countryCode"]].value_counts())
        df_viz.reset_index(inplace=True)
        df_viz.columns = ['Full_name', 'Country_code', 'Suspicious_order_count']
        countries = {}
        for country in pycountry.countries:
            countries[country.alpha_2] = country.name
        df_viz['Country_code'] = df_viz.Country_code.apply(lambda x: countries.get(x, 'Unknown code'))
    except Exception:
        print("Sorry, Something went wrong in predictions_of_fraud")
        exit("return code 1")

    return df_viz





def main():
    csv_path='/home/siddharth/Downloads/DataEngineerTest/traders_data.csv'
    stock_syb='AMZN'
    start_date='2020-02-01'
    end_date='2020-03-31'
    data_traders=read_data(csv_path,start_date,end_date,stock_syb)
    data_yahoo=get_yahoo_data(stock_syb,start_date,end_date)
    data_cleaned = data_cleaning(data_traders)
    predictions_of_fraud(data_cleaned,data_yahoo).to_csv('/home/siddharth/Downloads/DataEngineerTest/suspicious_order.csv')
if __name__ == "__main__":
    main()


